/*
Comments:
1. I removed the yellow limits in the parsing function with the idea that for this project
   it was not needed but if future features needed them we could easily placed them back in.
2. I was not clear if we where allowed to use other libraries so I kept to using only built in native
   libriaries. Likely with the use of other opensource libraries there my be a way to clean this up some more.
3. I played with the idea of breaking up my parsing function to demonstrate how we could write to future proof
   if the examples was real and be able to make more readable but I think for this challenge this was the most
   effective way to solve without adding too much complexity
4. Finally I did my best to keep the complexity at an O(n) - did take me a few trys to be honest :) but hopefully
   this solution works.   
*/

/*
Imports
*/
const fs = require('fs')

/*
Function: Parses the lines from the input file looking for the requiered RED High/Lows
Input: Array of Stings -  Lines from input file
Output: Array of Objects - Matching Alerts for TSTAT and BATT
*/
function parseMessages (messages) {
  const satelliteAlerts = []
  const componentAlerts = { BATT: {}, TSTAT: {} }

  messages.forEach((message) => {
    const msg = {}
    const alert = {}

    const alertMsg = message.split('|')

    // Assigning Parsed values from each line to a object so we can manuiplate and compare
    msg.timestamp = new Date(alertMsg[0].replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3'))   // Formating the date string to add dashes to make it readable by Date() 
    msg.id = parseInt(alertMsg[1])
    msg.component = alertMsg[7]
    msg.raw = parseFloat(alertMsg[6])
    msg.red_low_limit = parseFloat(alertMsg[5])
    msg.red_high_limit = parseFloat(alertMsg[2])

    if (msg.component === 'BATT') {
      msg.severity = 'RED LOW'
    } else {
      msg.severity = 'RED HIGH'
    }

    //Core logic for figuring out if a message should be reported or not.
    if ((msg.component === 'BATT' && msg.raw < msg.red_low_limit) || (msg.component === 'TSTAT' && msg.raw > msg.red_high_limit)) {
      if (!componentAlerts[msg.component][msg.id]) {
        componentAlerts[msg.component][msg.id] = {}
        componentAlerts[msg.component][msg.id].last_ts = msg.timestamp
        componentAlerts[msg.component][msg.id].count = 1
        componentAlerts[msg.component][msg.id].alerted = false
      } else if (msg.timestamp.getTime() - componentAlerts[msg.component][msg.id].last_ts.getTime() < 300000) {
        componentAlerts[msg.component][msg.id].count++
      }

      if (componentAlerts[msg.component][msg.id].count > 2 && componentAlerts[msg.component][msg.id].alerted === false) {
        alert.satelliteId = msg.id
        alert.severity = msg.severity
        alert.component = msg.component
        alert.timestamp = componentAlerts[msg.component][msg.id].last_ts.toISOString()
        satelliteAlerts.push(alert)
        componentAlerts[msg.component][msg.id].alerted = true
      }

      if (msg.timestamp.getTime() - componentAlerts[msg.component][msg.id].last_ts.getTime() > 300000) {
        componentAlerts[msg.component][msg.id].last_ts = msg.timestamp
        componentAlerts[msg.component][msg.id].count = 1
        componentAlerts[msg.component][msg.id].alerted = false
      }
    }
  })

  return satelliteAlerts
}

/*
Read the specified input file into an Array, each element in the array is
identified by either a carrage return or new line.
*/
try {
  const messages = fs.readFileSync('input.file', 'UTF-8').split(/\r?\n/)

  const satAlerts = JSON.stringify(parseMessages(messages), null, 4)

  console.log(satAlerts)
  
} catch (err) {
  console.error(err)
}